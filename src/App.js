import "./App.css";

function App() {
  return (
    <div className="App">
      <html lang="en">
        <body>
          <div class="smooth">
            <header>
              <h3>Tama</h3>
              <ul>
                <li>
                  <a href="#atas">Home</a>
                </li>
                <li>
                  <a href="#tengah">My Services</a>
                </li>
                <li>
                  <a href="#bawah">Experiences</a>
                </li>
              </ul>
            </header>
            <section id="atas">
              <div class="content parallax">
                <div>
                  <h1>P.F. Utama</h1>
                  <svg class="title">
                    <text x="0" y="40">
                      P.F. Utama
                    </text>
                    <path d="M 0 66 50 61"></path>
                  </svg>
                  <p class="lead">
                    Hello There! I am Pribadi Fitra Utama but Tama will do just
                    fine. I work as freelance kit designer at Indonesian Based
                    Printing House called Axia Creative co. and is also
                    undergoing bootcamp as a front-end developer at Glints-Binar
                    Academy.
                  </p>
                </div>
                <div class="blur">
                  <form>
                    <img src="./profpic.jpg" />
                    <input type="text" placeholder="Your Email:" />
                    <input type="text" placeholder="Your Message:" />
                    <a class="button">Email Me</a>
                  </form>
                </div>
              </div>
            </section>
            <section id="tengah">
              <h2>
                My<span> Services</span>
              </h2>
              <p>
                I open a commission to work on your translation needs from
                Indonesian to English and vice versa as well as making soccer or
                futsal jerseys.
              </p>
              <div class="blocks">
                <div class="block">
                  <img src="https://cdn.iconscout.com/icon/free/png-512/google-translate-3-555703.png" />
                </div>
                <div class="block">
                  <img src="https://image.flaticon.com/icons/png/512/63/63297.png" />
                </div>
                <div class="block">
                  <img src="https://img.icons8.com/ios/452/adobe-photoshop.png" />
                </div>
                <div class="block">
                  <img src="https://www.clipartmax.com/png/full/158-1588263_%C2%A0-soccer-ball-with-no-white-background.png" />
                </div>
              </div>
              <p>
                If you are interested in using my services, you can contact me
                via the email box above. Just scroll up this page and you will
                find it.
              </p>
            </section>
            <section id="bawah">
              <div class="grid g_two">
                <div>
                  <h2>
                    My<span> Experiences</span>
                  </h2>
                  <p>
                    [Freelance] English-Indonesian Translator: Translate various
                    types of subjects from ID to EN and vice versa, Proofread
                    various fields of business documents & Academic Journals,
                    Create scripts for social media advertisement (IG ads).{" "}
                  </p>
                  <p>
                    [Freelance] Translator for Cordova Translation: Translate
                    documents in the fields of legal and technical instruments,
                    Cooperate with several translators in completing journal
                    translation
                  </p>
                  <p>
                    [Intern] Assistant Script Editor & Broadcasting for KompasTV
                    Dewata: Transcribe narrative scripts from the interview of
                    local interviews, Assist the camera persons in preparing and
                    setting up recording devices
                  </p>
                </div>
                <div class="flex column">
                  <div class="flex column">
                    <div class="flex jcsb">
                      <h4>Communication</h4>
                      <span>77%</span>
                    </div>
                    <progress value="77" max="100"></progress>
                  </div>
                  <div class="flex column">
                    <div class="flex jcsb">
                      <h4>Teamwork</h4>
                      <span>85%</span>
                    </div>
                    <progress value="85" max="100"></progress>
                  </div>
                  <div class="flex column">
                    <div class="flex jcsb">
                      <h4>Marketing</h4>
                      <span>70%</span>
                    </div>
                    <progress value="70" max="100"></progress>
                  </div>
                  <div class="flex column">
                    <div class="flex jcsb">
                      <h4>Design</h4>
                      <span>68%</span>
                    </div>
                    <progress value="68" max="100"></progress>
                  </div>
                </div>
              </div>
            </section>
          </div>
        </body>
      </html>
    </div>
  );
}

export default App;
